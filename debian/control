Source: libtest-minimumversion-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Danjean <vdanjean@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Salvatore Bonaccorso <carnil@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libfile-find-rule-perl <!nocheck>,
                     libfile-find-rule-perl-perl <!nocheck>,
                     libperl-minimumversion-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtest-minimumversion-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtest-minimumversion-perl.git
Homepage: https://metacpan.org/release/Test-MinimumVersion
Rules-Requires-Root: no

Package: libtest-minimumversion-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libfile-find-rule-perl,
         libfile-find-rule-perl-perl,
         libperl-minimumversion-perl
Description: Perl test module to check the version of perl required
 Test::MinimumVersion is a simple test to look at the features you are using in
 your code and determine the minimum version of the perl interpreter that is
 required. This is helpful, for example, when you are using features like 'say'
 which was introduced in Perl 5.10.
 .
 This test makes it easy to determine if your Perl code requires a newer perl
 than expected, or if you accidentally made your dist require a newer version
 than strictly necessary.
